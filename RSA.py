from scenarios.BlindingScenario import BlindingScenario
from scenarios.BruteForceFactorization import BruteForceFactorization
from scenarios.CommonModulusScenario import CommonModulusScenario
from scenarios.FranklinReiterScenario import FranklinReiterScenario
from scenarios.WienerScenario import WienerScenario
from scenarios.RandomFaultScenario import RandomFaultScenario
from scenarios.HastadScenario import HastadScenario
from scenarios.TimingScenario import TimingScenario

scenarios = [BruteForceFactorization(),
             BlindingScenario(),
             CommonModulusScenario(),
             WienerScenario(),
             HastadScenario(),
             FranklinReiterScenario(),
             RandomFaultScenario(),
             TimingScenario()
             ]

if __name__ == '__main__':
    for sc in scenarios:
        print(f'Running {sc.name()}')
        sc.perform()
        print('SUCCESS\n')
