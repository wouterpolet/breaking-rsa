import time

from keygen.generation import low_exponent_gen_pq, low_exponent_gen_ed, key_gen
from attack.WienerAttack import WienerAttack
from scenarios.Scenario import Scenario


class WienerScenario(Scenario):

    def name(self):
        return 'Wiener Attack'

    def generate_params(self):
        msg = ["Generating keys with low private exponent d..."]
        gen_pq = low_exponent_gen_pq
        gen_ed = low_exponent_gen_ed
        # generate public/private key
        n, e, d = key_gen(512, gen_pq, gen_ed)
        msg.append("N = " + str(n) + "\ne = " + str(e) + "\nd = " + str(d))
        a = "Generated public and private keys:\n"
        a += "N = " + str(n) + "\n"
        a += "e = " + str(e) + "\n"
        a += "d = " + str(d) + "\n"
        return [n, e, d], a, msg

    def perform(self):
        [n, e, d], _, msg = self.generate_params()
        attack = WienerAttack()
        attack.set_parameters({'N': n, 'e': e})
        msg.append("Starting the attack: finding private key d...")
        d1, _, messages_to_print = attack.decrypt()
        for i in messages_to_print:
            if i != '':
                msg.append(i)
        assert d == d1
        msg.append("Found private key d = " + str(d1) + "!")
        return msg

    def evaluate(self):
        attack = WienerAttack()
        gen_pq = low_exponent_gen_pq
        gen_ed = low_exponent_gen_ed
        for key_size in [32, 64, 128, 256, 512, 1024]:
            params = []
            messages = []
            # average across 10 runs
            for _ in range(10):

                n, e, d = key_gen(key_size, gen_pq, gen_ed)
                params.append({'e': e})
                messages.append(n)
            results, times = attack.measure(messages, params)
            avg = sum(times) / len(times)
            print(f'{key_size}\t{avg}')
