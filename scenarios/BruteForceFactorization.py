import random as rn

from attack.BruteForceFactoring import BruteForceFactoring
from keygen.generation import random_gen_pq, random_gen_ed, key_gen
from rsa.decryption import decrypt
from rsa.encryption import encrypt
from rsa.signing import sign, verify_signature
from scenarios.Scenario import Scenario
from util.arithmetic import basic_factor, fermat_factor, pollard_rho_factor, quad_sieve_factor


class BruteForceFactorization(Scenario):

    def name(self):
        return 'Brute Force Factorization'

    def generate_params(self):
        gen_pq = random_gen_pq
        gen_ed = random_gen_ed

        # generate public/private key
        n, e, d = key_gen(32, gen_pq, gen_ed)
        msg = ["Generating keys and random message..."]
        # random message m
        m = rn.randint(0, n)
        msg.append("N = " + str(n) + "\ne = " + str(e) + "\nd = " + str(d) + "\nm = " + str(m))
        # encrypt with public key
        msg.append("Encrypting the message...")
        c = encrypt(m, e, n)
        msg.append("c = " + str(c))

        a = "Generated public key:\n"
        a += "N = " + str(n) + "\n"
        a += "e = " + str(e) + "\n"
        a += "d = " + str(d) + "\n"
        a += "m = " + str(m) + "\n"
        a += "c = " + str(c) + "\n"
        return [n, e, d, m, c], a, msg

    def perform(self):
        name = "pollard rho"
        [n, e, d, m, c], _, msg = self.generate_params()
        # check decryption with private key is correct
        assert m == decrypt(c, d, n)
        # check attack recovers private key from public key
        attack = BruteForceFactoring()
        attack.set_parameters({'N': n, 'e': e, 'method': name})
        d1, _, messages_to_print = attack.decrypt()
        for i in messages_to_print:
            if i != '':
                msg.append(i)
        assert d == d1
        msg.append("d = " + str(d1))
        return msg



    def evaluate(self):
        factoring_method_and_keysize = [(basic_factor, [16, 32, 48]),
                                        (fermat_factor, [16, 32, 48]),
                                        (pollard_rho_factor, [16, 32, 48, 64, 80]),
                                        (quad_sieve_factor, [16, 32, 48, 64, 80])]

        attack = BruteForceFactoring()
        gen_pq = random_gen_pq
        gen_ed = random_gen_ed

        for fact, key_sizes in factoring_method_and_keysize:
            print(fact.__name__)
            for key_size in key_sizes:
                ns, params = [], []
                for _ in range(10):

                    n, e, d = key_gen(key_size, gen_pq, gen_ed)

                    ns.append(n)
                    params.append({'e': e, 'factorize': fact})

                results, times = attack.measure(ns, params)
                avg = sum(times) / len(times)
                print(f'{key_size}\t{avg}')
