import hashlib
import random as rn

from attack.BlindingAttack import BlindingAttack
from keygen.generation import random_gen_pq, random_gen_ed, key_gen
from rsa.decryption import decrypt
from rsa.encryption import encrypt
from rsa.signing import sign, verify_signature
from scenarios.Scenario import Scenario


class BlindingScenario(Scenario):

    def name(self):
        return 'Blinding Attack'

    def generate_params(self):
        gen_pq = random_gen_pq
        gen_ed = random_gen_ed
        msg = []
        # generate public/private key
        n, e, d = key_gen(32, gen_pq, gen_ed)
        msg.append("Generating random keys and message...")
        # random message m
        m = rn.randint(0, n)
        msg.append("N = " + str(n) + "\ne = " + str(e) + "\nd = " + str(d) + "\nm =" + str(m))
        # cryptographically hash the message
        msg.append("Hashing the message...")
        m = hashlib.sha256(str.encode(str(m))).digest()
        msg.append("m = " + str(m))

        # integer representation of the hash digest
        msg.append("Converting the hashed message to integer...")
        m = int.from_bytes(m, "big") % n
        msg.append("m = " + str(m))
        # encrypt with public key
        msg.append("Encrypting and signing the message...")
        c = encrypt(m, e, n)
        msg.append("c = " + str(c))
        # sign message
        msg.append("Signing the message using private key d...")
        s = sign(m, d, n)
        msg.append("s = " + str(s))
        a = "Generated public key:\n"
        a += "N = " + str(n) + "\n"
        a += "e = " + str(e) + "\n"
        a += "d = " + str(d) + "\n"
        a += "m = " + str(m) + "\n"
        a += "c = " + str(c) + "\n"
        a += "s = " + str(s) + "\n"
        return [n, e, d, m, c, s], a, msg

    def perform(self):
        [n, e, d, m, c, s], a, msg = self.generate_params()
        # verify signature
        assert verify_signature(s, m, e, n)
        # check decryption with private key is correct
        assert m == decrypt(c, d, n)

        attack = BlindingAttack()
        msg.append("Starting the attack: finding the signature given public key and the signature function...")
        attack.set_parameters({'N': n, 'e': e, 'M': m, 'd': d})
        s1, _, messages_to_print = attack.decrypt()
        for i in messages_to_print:
            if i != '':
                msg.append(i)
        msg.append("Found signature s = " + str(s1))
        assert s == s1
        return msg

    def evaluate(self):
        gen_pq = random_gen_pq
        gen_ed = random_gen_ed
        attack = BlindingAttack()

        for key_size in [32, 64, 128, 256, 512, 1024, 2048]:
            params = []
            messages = []
            # average across 10 runs
            for _ in range(10):
                n, e, d = key_gen(key_size, gen_pq, gen_ed)

                m = rn.randint(0, n)
                m = hashlib.sha256(str.encode(str(m))).digest()
                m = int.from_bytes(m, "big") % n

                messages.append(m)
                params.append({'n': n, 'e': e, 'sign': (lambda mess, mod: sign(mess, d, mod))})

            results, times = attack.measure(messages, params)
            avg = sum(times) / len(times)
            print(f'{key_size}\t{avg}')
