import random as rn

from keygen.generation import random_gen_pq, random_gen_ed, key_gen_pq
from attack.RandomFaultAttack import RandomFaultAttack
from scenarios.Scenario import Scenario


class RandomFaultScenario(Scenario):

    def name(self):
        return 'Random Fault Attack'

    def generate_params(self):
        gen_pq = random_gen_pq
        gen_ed = random_gen_ed
        # generate public/private key
        n, e, d, p, q = key_gen_pq(256, gen_pq, gen_ed)
        msg = ["Generating keys and random message to encrypt..."]
        m = rn.randint(0, n)
        msg.append("N = " + str(n) + "\ne = " + str(e) + "\nd = " + str(d) + "\nm = " + str(m))
        a = "Generated public and private keys:\n"
        a += "p = " + str(p) + "\n"
        a += "q = " + str(q) + "\n"
        a += "N = " + str(n) + "\n"
        a += "e = " + str(e) + "\n"
        a += "d = " + str(d) + "\n"
        a += "m = " + str(m) + "\n"
        return [n, e, d, m, p, q], a, msg

    def perform(self):
        [n, e, d, m, p, q], _, msg = self.generate_params()
        attack = RandomFaultAttack()
        msg.append("Started the attack: finding the prime p, given the public key and a faulty "
                   "signature function...")
        attack.set_parameters({'N': n, 'e': e, 'M': m, 'd': d, 'p' : p, 'q' : q})
        p_prime, _, msj = attack.decrypt()
        for i in msj:
            if i != '':
                msg.append(i)
        msg.append("Found p!")
        msg.append("p = " + str(p_prime))
        assert {p, q} == {p_prime, n // p_prime}
        return msg

    def evaluate(self):
        attack = RandomFaultAttack()
        gen_pq = random_gen_pq
        gen_ed = random_gen_ed
        for key_size in [32, 64, 128, 256, 512, 1024, 2048]:
            params = []
            messages = []
            # average across 10 runs
            for _ in range(10):
                # generate public/private key
                n, e, d, p, q = key_gen_pq(key_size, gen_pq, gen_ed)
                m = rn.randint(0, n)
                params.append({'n': n, 'e': e, 'sign': (lambda mess: faulty_sign(mess, d, p, q))})
                messages.append(m)

            results, times = attack.measure(messages, params)
            avg = sum(times) / len(times)
            print(f'{key_size}\t{avg}')