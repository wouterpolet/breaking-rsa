import abc


class Scenario(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def name(self):
        pass

    @abc.abstractmethod
    def perform(self) -> any:
        pass

    @abc.abstractmethod
    def generate_params(self):
        pass

    @abc.abstractmethod
    def evaluate(self):
        pass
