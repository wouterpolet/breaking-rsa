import math

from keygen.generation import random_gen_pq, fixed_e_gen_ed , key_gen
from attack.HastadAttack import HastadAttack
from scenarios.Scenario import Scenario
from rsa.encryption import encrypt
import random as rn


class HastadScenario(Scenario):

    def name(self):
        return 'Hastad Attack'

    def generate_params(self):
        e = 3
        participants = e

        gen_pq = random_gen_pq
        gen_ed = fixed_e_gen_ed(e)
        msg = [f"Generating {participants} key pairs with shared encryption exponent {e}..."]
        keys = [key_gen(16, gen_pq, gen_ed) for _ in range(participants)]

        ns = [n for n, _, _ in keys]
        m = rn.randint(0, min(ns))
        cs = [encrypt(m, e, n) for n, _, _ in keys]
        a = "Generated public and private keys:\n"
        a += "e = " + str(e) + "\n"
        a += "ns = " + str(ns) + "\n"
        a += "m = " + str(m) + "\n"
        a += "cs = " + str(cs) + "\n"
        msg.append("e = " + str(e) + "\nns = " + str(ns) + "\nm = " + str(m) + "\ncs = " + str(cs))
        return [e, ns, cs, m], a, msg

    def perform(self):

        [e, ns, cs, m], _, msg = self.generate_params()
        participants = e
        msg.append('Broadcasting a message to the receivers...')
        attack = HastadAttack()
        msg.append(f"Attacker observed {participants} ciphertexts!")
        attack.set_parameters({'ns': ns, 'cs': cs, 'e': e})
        m1, _, messages_to_print = attack.decrypt()
        for i in messages_to_print:
            if i != '':
                msg.append(i)
        assert m == m1
        return msg

    def evaluate(self):

        attack = HastadAttack()

        for e in [3, 5, 7, 11, 13, 17]:
            print(f'e = {e}')
            participants = e
            gen_pq = random_gen_pq
            gen_ed = fixed_e_gen_ed(e)
            for key_size in [32, 64, 128, 256, 512, 1024]:
                params = []
                messages = []
                # average across 10 runs
                for _ in range(10):

                    keys = [key_gen(key_size, gen_pq, gen_ed) for _ in range(participants)]

                    ns = [n for n, _, _ in keys]
                    m = rn.randint(0, min(ns))
                    cs = [encrypt(m, e, n) for n, _, _ in keys]

                    messages.append(m)
                    params.append({'ns': ns, 'cs': cs, 'e': e})

                results, times = attack.measure(messages, params)
                avg = sum(times) / len(times)
                print(f'{key_size}\t{avg}')

