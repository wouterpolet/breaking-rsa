from attack.CommonModulusAttack import CommonModulusAttack
from keygen.generation import key_gen_common_modulus, random_gen_ed, random_gen_pq
from scenarios.Scenario import Scenario


class CommonModulusScenario(Scenario):

    def name(self):
        return 'Common Modulus Attack'

    def generate_params(self):
        gen_pq = random_gen_pq
        gen_ed = random_gen_ed
        n, e1, d1, e2, d2 = key_gen_common_modulus(256, gen_pq, gen_ed)
        msg = ["Generating keys using common modulus N...", "Generated common modulus \nN = " + str(n),
               "Generated two public keys with the same modulus N: \ne1 = " + str(e1) + "\ne2 = " + str(e2),
               "Generated corresponding private keys: \nd1 = " + str(d1) + "\nd2 = " + str(d2)]
        a = "Generated public and private keys:\n"
        a += "N = " + str(n) + "\n"
        a += "e1 = " + str(e1) + "\n"
        a += "d1 = " + str(d1) + "\n"
        a += "e2 = " + str(e2) + "\n"
        a += "d2 = " + str(d2) + "\n"
        return [n, e1, d1, e2, d2], a, msg

    def perform(self):
        [n, e1, d1, e2, d2], _, msg = self.generate_params()
        attack = CommonModulusAttack()
        attack.set_parameters({'N': n, 'e1': e1, 'e2': e2, 'd2': d2})
        d, _, messages_to_print = attack.decrypt()
        for i in messages_to_print:
            if i != '':
                msg.append(i)
        assert d1 == d
        msg.append("d1 = " + str(d1))
        return msg

    def evaluate(self):
        pass
