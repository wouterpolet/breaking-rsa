import random as rn

from attack.LowPublicExponentFranklinReiterAttack import FranklinReiterAttack
from keygen.generation import random_gen_pq, low_public_exponent_gen_ed, key_gen
from rsa.encryption import encrypt
from scenarios.Scenario import Scenario


class FranklinReiterScenario(Scenario):

    def name(self):
        return 'Franklin-Reiter Related Message Attack'

    def generate_params(self):
        msg = ["Generating keys..."]
        gen_pq = random_gen_pq
        gen_ed = low_public_exponent_gen_ed
        n, e, d = key_gen(32, gen_pq, gen_ed)
        msg.append("Generated public key with small public exponent e:\nN = " + str(n) + "\ne = " \
                   + str(e) + "\nd = " + str(d))
        # random linear function coefficients
        msg.append("Generating random linear function...")
        a_coeff = rn.randint(0, n)
        b = rn.randint(1, n)
        msg.append("The function is: " + str(a_coeff) + " * X + " + str(b))
        # random message m1
        msg.append("Generating two random messages m1 and m2 = a * m1 + b...")
        m1 = rn.randint(0, n)
        m2 = (a_coeff * m1 + b) % n
        msg.append("The messages are:\nm1 = " + str(m1) + "\nm2 = " + str(m2))
        # encrypt with public key
        c1 = encrypt(m1, e, n)
        c2 = encrypt(m2, e, n)
        msg.append("Encrypting m1 and m2...")
        msg.append("c1 = " + str(c1) + "\nc2 = " + str(c2))
        a = "Generated public and private keys:\n"
        a += "N = " + str(n) + "\n"
        a += "e = " + str(e) + "\n"
        a += "d = " + str(d) + "\n"
        a += "a = " + str(a_coeff) + "\n"
        a += "b = " + str(b) + "\n"
        a += "m1 = " + str(m1) + "\n"
        a += "m2 = " + str(m2) + "\n"
        a += "c1 = " + str(c1) + "\n"
        a += "c2 = " + str(c2) + "\n"
        return [n, e, d, a_coeff, b, m1, m2, c1, c2], a, msg

    def perform(self):
        [n, e, d, a, b, m1, m2, c1, c2], _, msg = self.generate_params()
        attack = FranklinReiterAttack()
        # print("Given N, e, the linear function used to find m2 and the ciphertext, one can find m1...")
        attack.set_parameters({'N': n, 'e': e, 'a': a, 'b': b, 'c1': c1, 'c2': c2})

        m3, a, messages_to_print = attack.decrypt()
        # print("Found m1 =", m3)
        assert m1 == m3
        for i in messages_to_print:
            msg.append(i)
        msg.append(a + str(m3))
        return msg
        # except:
        #     print("Franklin-Reiter Attack failed. Factorizing N is possible for these keys (too small). ")

    def evaluate(self):
        pass