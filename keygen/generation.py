import random as rn

from util.arithmetic import is_fermat_prime, eea, nth_root


def gen_prime(b):
    """uniform random 'probably prime' number of b bits"""
    while True:
        x = rn.randint(1 << b, 1 << b + 1)
        if is_fermat_prime(x):
            return x


def random_gen_pq(b):
    p = gen_prime(b // 2)
    q = gen_prime(b - b // 2)
    return (p, q) if p != q else random_gen_pq(b)


def random_gen_ed(p, q):
    """uniform random number co-prime with phi, and its inverse mod phi"""
    phi = (p - 1) * (q - 1) if p != q else p * (p - 1)
    while True:
        e = rn.randint(2, phi)
        gcd, _, d = eea(e, phi)
        if gcd == 1:
            return e, d % phi


def fixed_e_gen_ed(e):
    """e is 3"""

    def generator(p, q):
        phi = (p - 1) * (q - 1) if p != q else p * (p - 1)
        gcd, _, d = eea(e, phi)
        if gcd != 1:
            raise ValueError("Phi is divisible by e")
        return e, d % phi

    return generator


def key_gen(b, gen_pq, gen_ed):
    """generate RSA key (n, e, d)"""
    p, q = gen_pq(b)
    n = p * q
    try:
        e, d = gen_ed(p, q)
    except ValueError:
        return key_gen(b, gen_pq, gen_ed)
    return n, e, d


def key_gen_pq(b, gen_pq, gen_ed):
    """generate RSA key (n, e, d, p, q)"""
    p, q = gen_pq(b)
    n = p * q
    e, d = gen_ed(p, q)
    return n, e, d, p, q


def key_gen_common_modulus(b, gen_pq, gen_ed):
    """generate RSA key using common modulo between two different users"""
    p, q = gen_pq(b)
    n = p * q
    e1, d1 = gen_ed(p, q)
    e2, d2 = gen_ed(p, q)
    return n, e1, d1, e2, d2


def low_exponent_gen_pq(b):
    """generate primes p and q used for Wiener attack"""
    while True:
        p, q = random_gen_pq(b)
        if p > q:
            p, q = q, p
        if p < 2 * q:
            return p, q


def low_exponent_gen_ed(p, q):
    """generate e and d, with d < (N^(0.25))/3"""
    n = p * q
    phi = (p - 1) * (q - 1) if p != q else p * (p - 1)
    while True:
        try:
            d = rn.randint(2, min(phi, int(n ** 0.25 / 3)))
        except OverflowError:
            d = rn.randint(2, min(phi, nth_root(n, 4) // 3))
        gcd, _, e = eea(d, phi)
        if gcd == 1:
            return e % phi, d


def low_public_exponent_gen_ed(p, q):
    phi = (p - 1) * (q - 1) if p != q else p * (p - 1)
    while True:
        e = rn.randint(2, 1000)
        gcd, _, d = eea(e, phi)
        if gcd == 1:
            return e, d % phi
