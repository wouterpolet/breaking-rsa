from sympy.ntheory.modular import crt
import random as rn
import time

SQ_TIME = 1
MUL_TIME = 2


def mul_simulation_time(m):
    rn.seed(m)
    t = max(0, rn.gauss(MUL_TIME, 1))
    rn.seed(time.time_ns())
    return t


def sign(m, d, n):
    """sign a message using private key d"""
    return pow(m, d, n)


def timed_sign(m, d, n):
    """sign a message using private key d and measure (simulated) time of computation by square and multiply"""
    if d == 0:
        return 1, 0
    elif d % 2 == 0:
        result, t = timed_sign(pow(m, 2, n), d // 2, n)
        t += max(0, rn.gauss(SQ_TIME, 1))
    else:
        result, t = timed_sign(m, d - 1, n)
        result = (m * result) % n
        t += mul_simulation_time(m)
    return result, t


def verify_signature(s, m, e, n):
    """verify the signature of a message using the public key e"""
    m_prime = pow(s, e, n)
    return m_prime == m


def faulty_sign(m, d, p, q):
    """a faulty implementation of the sign function where 1 bit is flipped"""
    sign_p = pow(m, (d % (p - 1)), p)
    sign_q = pow(m, (d % (q - 1)), q)
    t1, _ = crt([p, q], [1, 0])
    t2, _ = crt([p, q], [0, 1])
    return t1*sign_p + (sign_q ^ 1)*t2
