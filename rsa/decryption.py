def decrypt(c, d, n):
    """decrypt ciphertext c using private key d"""
    return pow(c, d, n)
