def encrypt(m, e, n):
    """encrypt plaintext message m using public key e"""
    return pow(m, e, n)
