# Breaking RSA

This repository contains the code that was created by Yana Angelova, Dan Andreescu, Ioana Savu, and Wouter Polet for the Hacking Lab course at the TU Delft.

It contains implementations of various attacks on the RSA encryption scheme.
Furthermore it provides a GUI to run these attacks with various parameters.
Launching the GUI is done by executing the `App.py` file in the `gui` package.
Make sure to first install the requirements from the `requirements.txt`.
To do so, you can run:
```bash
pip install -r requirements.txt
```
