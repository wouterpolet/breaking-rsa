from math import gcd

from attack.Attack import Attack
from rsa.signing import faulty_sign


class RandomFaultAttack(Attack):

    def __init__(self):
        self.N = None
        self.e = None
        self.M = None
        self.d = None
        self.p = None
        self.q = None
        self.sign = (lambda mess: faulty_sign(mess, self.d, self.p, self.q))

    def name(self):
        return 'Random Fault Attack'

    def needed_parameters(self) -> dict[str, str]:
        return {
            'N': 'modulus of RSA keys',
            'e': 'public encryption key',
            'M': 'message to sign',
            'd': 'private decryption key',
            'p': 'prime factor of N',
            'q': 'prime factor of N'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        setattr(self, 'N', int(params['N']))
        setattr(self, 'e', int(params['e']))
        setattr(self, 'M', int(params['M']))
        setattr(self, 'd', int(params['d']))
        setattr(self, 'p', int(params['p']))
        setattr(self, 'q', int(params['q']))

    def decrypt(self) -> (int, any, any):
        s = self.sign(self.M)
        g = gcd(pow(s, self.e, self.N) - self.M, self.N)
        return g, 'Found prime factor p: ', ['']
