from attack.Attack import Attack
import random as rn
from rsa.signing import mul_simulation_time, timed_sign


class TimingAttack(Attack):

    def __init__(self):
        self.iterations = None
        self.N = None
        self.d = None
        self.timed_sign = (lambda mess: timed_sign(mess, self.d, self.N))

    def needed_parameters(self) -> dict[str, str]:
        return {
            'iterations': 'iterations per bit of key',
            'N': 'public modulus',
            'd': 'private decryption key'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        setattr(self, 'iterations', int(params['iterations']))
        setattr(self, 'N', int(params['N']))
        setattr(self, 'd', int(params['d']))

    def name(self):
        return 'Timing Attack'

    def decrypt(self) -> (int, any, any):
        msg = [f"Attacker measuring {self.iterations} signatures on random messages for each bit of the key..."]
        print(f"Attacker measuring {self.iterations} signatures on random messages for each bit of the key...")
        corr = [self.bit_correlation(i) for i in range(self.N.bit_length())]
        print("Attacker correlating signing times with multiplication times...")
        msg.append("Attacker correlating signing times with multiplication times...")
        mean = sum(corr) / self.N.bit_length()
        print("Attacker guessing bits by separating high vs low correlations...")
        msg.append("Attacker guessing bits by separating high vs low correlations...")
        bits = [1 if c > mean else 0 for c in corr]

        result = 0
        for i, b in enumerate(bits):
            result |= b << i
        msg.append('Found key ' + str(result))
        return result, 'Found key: ', msg

    def bit_correlation(self, i):
        ms = [rn.randint(0, self.N) for _ in range(self.iterations)]

        Ts = [self.timed_sign(mi)[1] for mi in ms]
        ts = [mul_simulation_time(pow(mi, 1 << i, self.N)) for mi in ms]

        mean_T = sum(Ts) / self.iterations
        mean_t = sum(ts) / self.iterations

        corr = 0
        for i in range(self.iterations):
            corr += (Ts[i] - mean_T) * (ts[i] - mean_t)

        return corr / self.iterations
