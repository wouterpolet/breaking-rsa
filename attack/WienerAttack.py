import math

from util.arithmetic import ea_fractions, frac_conversion
from attack.Attack import Attack


class WienerAttack(Attack):

    def __init__(self):
        self.N = None
        self.e = None

    def needed_parameters(self) -> dict[str, str]:
        return {
            'N': 'modulus of RSA keys',
            'e': 'public encryption key'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        setattr(self, 'N', int(params['N']))
        setattr(self, 'e', int(params['e']))

    def name(self):
        return 'Wiener Attack'

    def decrypt(self) -> (int, any, any):
        m = []
        qs = ea_fractions(self.e, self.N)
        m.append("Converting the continuous fraction to a rational number...")
        for i in range(1, len(qs)):
            if i % 2 == 0:
                qs_prime = qs.copy()[0:i+1]
                qs_prime[-1] += 1
                k, dg = frac_conversion(qs_prime)
            else:
                k, dg = frac_conversion(qs[0:i+1])
            phi = (self.e * dg) // k
            g = (self.e * dg) % k
            if phi == 0:
                continue
            a = (self.N - phi + 1) // 2
            b = pow(a, 2) - self.N
            if b != pow(int(math.isqrt(b)), 2):
                continue
            m.append('Found private key d!')
            return dg // g, 'Found private key d: ', m
        raise RuntimeError("Could not derive d")
