import abc
import time


class Attack(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def needed_parameters(self) -> dict[str, str]:
        pass

    def set_parameters(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        for key in params:
            setattr(self, key, params[key])

    @abc.abstractmethod
    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        pass

    @abc.abstractmethod
    def decrypt(self) -> (int, any, any):
        pass

    @abc.abstractmethod
    def name(self):
        pass

    def measure(self, messages: [int], params_list: [dict[str, any]]) -> ([int], [int]):
        results, times = [], []
        for m, params in zip(messages, params_list):
            start = time.time()
            self.set_parameters(params)
            result = self.decrypt(m)
            delta = time.time() - start
            results.append(result)
            times.append(delta * 1000)  # count milliseconds
        return results, times
