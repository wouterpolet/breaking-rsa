from util.arithmetic import eea, basic_factor, fermat_factor, pollard_rho_factor, quad_sieve_factor
from attack.Attack import Attack

factoring_method = {"brute force": basic_factor,
                    "fermat": fermat_factor,
                    "pollard rho": pollard_rho_factor,
                    "quadratic sieve": quad_sieve_factor}

class BruteForceFactoring(Attack):

    def __init__(self):
        self.N = None
        self.e = None
        self.method = None

    def name(self):
        return 'Brute Force Factorization'

    def get_factorization_options(self):
        return factoring_method.keys()

    def needed_parameters(self) -> dict[str, str]:
        return {
            'N': 'modulus of RSA keys',
            'e': 'public encryption key',
            'method': 'factorisation method to use: brute force, fermat, pollard rho, quadratic sieve'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        setattr(self, 'N', int(params['N']))
        setattr(self, 'e', int(params['e']))
        setattr(self, 'method', params['method'])

    def decrypt(self) -> (int, any, any):
        m = ['Started brute force attack: searching for private key d...']
        p, q = factoring_method[self.method](self.N)
        phi = (p - 1) * (q - 1) if p != q else p * (p - 1)
        _, _, d = eea(self.e, phi)
        return d % phi, 'Found private key d: ', m
