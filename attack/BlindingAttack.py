import random as rn
from attack.Attack import Attack
from rsa.signing import sign


class BlindingAttack(Attack):

    def __init__(self):
        self.N = None
        self.e = None
        self.M = None
        self.d = None
        self.sign = (lambda mess, mod: sign(mess, self.d, mod))

    def name(self):
        return 'Blinding Attack'

    def needed_parameters(self) -> dict[str, str]:
        return {
            'N': 'modulus of RSA keys',
            'e': 'public encryption key',
            'M': 'message to sign',
            'd': 'private decryption key'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        setattr(self, 'N', int(params['N']))
        setattr(self, 'e', int(params['e']))
        setattr(self, 'M', int(params['M']))
        setattr(self, 'd', int(params['d']))

    def decrypt(self) -> (int, any, any):
        r = rn.randint(0, self.N)
        m_prime = (pow(r, self.e, self.N) * self.M) % self.N
        s_prime = self.sign(m_prime, self.N)
        s = (s_prime * pow(r, -1, self.N)) % self.N
        return s, 'signature = ' + str(s), ['']
