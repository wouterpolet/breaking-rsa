import math

from util.arithmetic import eea, crt, nth_root
from attack.Attack import Attack
from rsa.decryption import decrypt


class HastadAttack(Attack):

    def __init__(self):
        self.ns = None
        self.cs = None
        self.e = None

    def name(self):
        return 'Hastad Attack'

    def needed_parameters(self) -> dict[str, str]:
        return {
            'ns': 'list of Ns corresponding to the observed cyphertexts',
            'cs': 'list of observed cyphertexts',
            'e': 'public encryption key'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        nss = params['ns'].split(',')
        ns = [int(n) for n in nss]
        css = params['cs'].split(',')
        cs = [int(c) for c in css]
        setattr(self, 'ns', ns)
        setattr(self, 'cs', cs)
        setattr(self, 'e', int(params['e']))

    def decrypt(self) -> (int, any, any):
        m = []
        allowed_duplicates = len(self.ns) - self.e
        # check is n's are relatively prime, so we can factor them
        m.append("Checking if Ns are relatively prime...")
        for i, n1 in enumerate(self.ns):
            for j, n2 in enumerate(self.ns):
                if i == j:
                    continue
                if n1 == n2:
                    if allowed_duplicates == 0:
                        m.append("Hastad attack fails if any 2 n's are equal")
                        raise ValueError("Hastad attack fails if any 2 n's are equal")
                    allowed_duplicates -= 1
                    continue
                gcd, _, _ = eea(n1, n2)
                if gcd != 1:
                    m.append("They are not!")
                    m.append("Factoring N since we found a divisor...")
                    p, q = gcd, n1 // gcd
                    phi = (p - 1) * (q - 1) if p != q else p * (p - 1)
                    _, _, d = eea(self.e, phi)
                    m.append("Private key obtained!")
                    m.append("Decrypting message with private key...")
                    dec = decrypt(self.cs[i], d % phi, n1)
                    m.append("Message decrypted! ")
                    return dec, "Decrypted message: ", m

        # if not, we can apply crt
        m.append("They are!")
        m.append("Applying CRT to find m^e mod a big big number...")
        x = crt(self.cs, self.ns)
        m.append("Computing e'th root of the result to decrypt message...")
        mess = nth_root(x, self.e)
        m.append("Decrypted message!")
        return mess, 'Decrypted message: ', m
