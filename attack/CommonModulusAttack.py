from util.arithmetic import is_fermat_prime, eea
from attack.Attack import Attack


def factorize_n_given_ed(n, e, d):
    k = e * d - 1
    for g in range(2, n - 1):
        if g == 2 or (g > 2 and is_fermat_prime(g)):
            t = k
            while t % 2 == 0:
                t = t // 2
                x = pow(g, t, n)
                if x > 1:
                    gcd, _, _ = eea(x - 1, n)
                    if gcd > 1:
                        p, q = gcd, n // gcd
                        return p, q
    raise RuntimeError('Could not factorize n given e and d')


class CommonModulusAttack(Attack):

    def __init__(self):
        self.N = None
        self.e1 = None
        self.e2 = None
        self.d2 = None

    def needed_parameters(self) -> dict[str, str]:
        return {
            'N': 'modulus of RSA keys',
            'e1': 'public encryption key 1',
            'e2': 'public encryption key 2',
            'd2': 'decryption key 2'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        setattr(self, 'N', int(params['N']))
        setattr(self, 'e1', int(params['e1']))
        setattr(self, 'e2', int(params['e2']))
        setattr(self, 'd2', int(params['d2']))

    def name(self):
        return 'Common Modulus Attack'

    def decrypt(self) -> (int, any, any):
        m = ['Started common modulus attack: searching for (p, q) given e2, d2...']
        # print('Started common modulus attack: searching for (p, q) given e2, d2...')
        p, q = factorize_n_given_ed(self.N, self.e2, self.d2)
        m.append('Found (p, q) = (' + str(p) + "," + str(q) + ")")
        # print('Found (p, q) = (', p, ",", q, ")")
        phi = (p - 1) * (q - 1) if p != q else p * (p - 1)
        m.append('Finding d1, the inverse of e1...')
        # print('Finding d1, the inverse of e1...')
        _, _, d1 = eea(self.e1, phi)
        m.append('Found private key d1!')
        return d1 % phi, 'Found private key d1: ', m
