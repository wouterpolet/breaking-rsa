from attack.Attack import Attack

from sympy.polys.domains import ZZ
from sympy.polys.galoistools import gf_gcd, gf_pow, gf_sub, gf_TC
# pip install sympy

def recover_message(n, e, a, b, c1, c2):
    g1 = [a, b]
    mess = ["Generating g1 = (a * x + b) ^ e - c2...", "Generating g2 = x ^ e - c1..."]
    g1 = gf_pow(ZZ.map(g1), e, n, ZZ)
    g2 = gf_pow(ZZ.map([1, 0]), e, n, ZZ)
    g1 = gf_sub(g1, ZZ.map([c2]), n, ZZ)
    g2 = gf_sub(g2, ZZ.map([c1]), n, ZZ)

    p = ZZ.map(g1)
    q = ZZ.map(g2)
    z = n
    mess.append("Computing GCD between g1 and g2...")
    g = gf_gcd(p, q, z, ZZ)
    mess.append("GCD FOUND!")
    m = gf_TC(g, ZZ)
    if len(g) > 2:
        raise Exception("Attack failed")
    mess.append('First message decrypted!')
    return (-m) % n, 'First message found: ', mess


class FranklinReiterAttack(Attack):

    def __init__(self):
        self.N = None
        self.e = None
        self.a = None
        self.b = None
        self.c1 = None
        self.c2 = None

    def name(self):
        return 'Franklin Reiter Attack'

    def needed_parameters(self) -> dict[str, str]:
        return {
            'N': 'modulus of RSA keys',
            'e': 'public encryption key',
            'a': 'first coefficient of the function',
            'b': 'second coefficient of the function',
            'c1': 'ciphertext 1',
            'c2': 'ciphertext 2'
        }

    def set_parameters_from_user_input(self, params: dict[str, any]) -> None:
        if not self.needed_parameters().keys() == params.keys():
            raise ValueError('Did not get the correct parameters for this attack.')
        setattr(self, 'N', int(params['N']))
        setattr(self, 'e', int(params['e']))
        setattr(self, 'a', int(params['a']))
        setattr(self, 'b', int(params['b']))
        setattr(self, 'c1', int(params['c1']))
        setattr(self, 'c2', int(params['c2']))

    def decrypt(self) -> (int, any, any):
        return recover_message(self.N, self.e, self.a, self.b, self.c1, self.c2)
