import math
import random as rn


def basic_factor(n):
    """factor n by checking all possible divisors"""
    for i in range(2, n // 2):
        if n % i == 0:
            return i, n // i
    raise RuntimeError('Could not decrypt by brute force')


def pollard_rho_factor(n, x0=2):
    """factor n by Pollard's rho method"""
    x, y, d = x0, x0, 1
    g = lambda x: (x * x + 1) % n
    while d == 1:
        x = g(x)
        y = g(g(y))
        d, _, _ = eea(abs(x - y), n)
    if d == n:
        return pollard_rho_factor(n, x0 + 1)
    else:
        return d, n // d


def fermat_factor(n):
    """factor n by Fermat's method"""
    b = 1
    while True:
        a2 = n + b * b
        a = math.isqrt(a2)
        if a ** 2 == a2:
            return a + b, a - b
        b += 1


def quad_sieve_factor(n, extra_sieve_size=5):
    """factor n with the quadratic sieve algorithm"""
    L = math.exp(math.sqrt(math.log(n) * math.log(math.log(n))))
    B = math.ceil(math.pow(L, 1 / math.sqrt(2))) + 1
    base_sieve = [True for _ in range(B + 1)]
    # find some primes
    for i in range(2, B + 1):
        if base_sieve[i]:
            for j in range(2 * i, B + 1, i):
                base_sieve[j] = False
    a = math.isqrt(n) + 1
    # prune factor base
    factor_base = []
    for p in range(3, B + 1):
        if base_sieve[p] and pow(n, p // 2, p) != 1:
            base_sieve[p] = False

    for p in range(2, B + 1):
        if base_sieve[p]:
            factor_base.append(p)

    smooth = []
    smooth_vectors = []
    # find smooth numbers and their factor base representation
    # at least 5 more smooth numbers than primes in basis, try more if it fails
    T = a
    while len(smooth) < len(factor_base) + extra_sieve_size:
        num = T * T - n
        if num > 2 * n:
            raise RuntimeError("Cannot factor through Quadratic Sieving, the public modulus might be too small for "
                               "this algorithm. Please try a different factorisation method")
        T += 1
        N = num
        factor_vector = [0 for _ in factor_base]
        for i, p in enumerate(factor_base):
            # write number is factor base
            while N % p == 0:
                factor_vector[i] ^= 1
                N //= p
                if N == 1:
                    smooth.append(num)
                    smooth_vectors.append(factor_vector)
                    break
            if N == 1:
                break
    # solve for null space
    kernel = z2_kernel(smooth_vectors)
    # check if we found a factor
    for result_vector in kernel:
        maybe_T = 1
        maybe_reduced_T = 1
        for base, exp in zip(smooth, result_vector):
            if exp % 2 == 1:
                maybe_T *= base + n
                maybe_reduced_T *= base
        maybe_T = math.isqrt(maybe_T)
        maybe_reduced_T = math.isqrt(maybe_reduced_T)
        gcd, _, _ = eea(n, maybe_T - maybe_reduced_T)
        if 1 < gcd < n:
            return gcd, n // gcd
    # retry with larger sieve if not found
    return quad_sieve_factor(n, 2 * extra_sieve_size)


def z2_kernel(A):
    """compute null space of a matrix in ring Z2"""
    # transpose
    B = [[A[j][i] for j in range(len(A))] for i in range(len(A[0]))]
    # augment with I
    for i in range(len(B[0])):
        row = [1 if i == j else 0 for j in range(len(B[0]))]
        B.append(row)
    # transpose augmented
    B = [[B[j][i] for j in range(len(B))] for i in range(len(B[0]))]
    # gauss elimination
    row_pivot = 0
    col_pivot = 0
    while row_pivot < len(B) and col_pivot < len(B[0]):
        # find pivot row and bubble up
        for row in range(row_pivot, len(B)):
            if B[row][col_pivot] == 1:
                B[row], B[row_pivot] = B[row_pivot], B[row]
                break
        # no more pivot rows, rank is smaller
        if B[row_pivot][col_pivot] == 0:
            col_pivot += 1
            continue
        # reduce following rows by pivot
        for row in range(0, len(B)):
            if row != row_pivot and B[row][col_pivot] == 1:
                for col in range(0, len(B[row])):
                    B[row][col] ^= B[row_pivot][col]
        row_pivot += 1
        col_pivot += 1
    # extract null space basis
    kernel = []
    for row in B:
        if sum(row[:len(A[0])]) == 0:
            kernel.append(row[len(A[0]):])
    return kernel


def eea(a, b):
    """extended euclid algorithm: computes (gcd(a,b), x, y) s.t. ax + by = gcd(a,b)"""
    s, s0, t, t0, r, r0 = 0, 1, 1, 0, b, a
    while r != 0:
        q = r0 // r
        s0, s = s, s0 - q * s
        t0, t = t, t0 - q * t
        r0, r = r, r0 - q * r
    return r0, t0, s0


def inverse(x, n):
    """inverse: computes the inverse of a number modulo n"""
    gcd, _, y = eea(x, n)
    if gcd != 1:
        raise Exception('modular inverse does not exist')
    else:
        return y % n


def crt(a, m):
    """solve mod system through chinese remainder theorem"""
    M = math.prod(m)
    x = 0
    for i in range(len(m)):
        Mi = M // m[i]
        y = eea(Mi, m[i])[2] % m[i]
        x += a[i] * Mi * y
        x %= M
    return x


def nth_root(x, n):
    """extract n-th integer root of x through binary search"""
    low, high = 0, x
    while True:
        mid = low + (high - low) // 2
        guess = pow(mid, n)
        if x == guess:
            break
        elif guess < x:
            low = mid
        else:
            high = mid
    return mid


def is_fermat_prime(n, k=64):
    """fermat primality test: n is composite with probability 1/2^k"""
    for _ in range(k):
        a = rn.randint(2, n - 1)
        if pow(a, n - 1, n) != 1:
            return False
    return True


def ea_fractions(a, b):
    """euclidean algorithm: computes the continuous fraction of a given rational number"""
    # print(f'Calculating the continuous fractions for {a} over {b} ')
    qs = []
    q = a // b
    qs.append(q)
    r = a % b
    while r:
        a, b = b, r
        qi = a // b
        r = a % b
        qs.append(qi)
    return qs


def frac_conversion(qs):
    """converts the continuous fraction to a fraction representation"""
    ns = []
    ds = []
    n0 = qs[0]
    ns.append(n0)
    d0 = 1
    ds.append(d0)
    n1 = qs[0]*qs[1] + 1
    ns.append(n1)
    d1 = qs[1]
    ds.append(d1)
    for i in range(2, len(qs)):
        ni = qs[i]*ns[i-1] + ns[i-2]
        ns.append(ni)
        di = qs[i]*ds[i-1] + ds[i-2]
        ds.append(di)
    return ns.pop(), ds.pop()
