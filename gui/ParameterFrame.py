from functools import partial
from tkinter import Text, StringVar, OptionMenu
import customtkinter

import scenarios
from attack import BlindingAttack, BruteForceFactoring, CommonModulusAttack, HastadAttack, RandomFaultAttack, \
    WienerAttack, FranklinReiterAttack, TimingAttack
from scenarios import BlindingScenario, BruteForceFactorization, CommonModulusScenario, HastadScenario, \
    RandomFaultScenario, WienerScenario, FranklinReiterScenario, TimingScenario

dict_attacks_scenarios = {
    BlindingAttack: BlindingScenario,
    BruteForceFactoring: BruteForceFactorization,
    CommonModulusAttack: CommonModulusScenario,
    HastadAttack: HastadScenario,
    RandomFaultAttack: RandomFaultScenario,
    WienerAttack: WienerScenario,
    FranklinReiterAttack: FranklinReiterScenario,
    TimingAttack: TimingScenario
}


class ParameterFrame(customtkinter.CTkFrame):
    def __init__(self, container):
        super().__init__(container)
        self.extra_label = None
        self.label = None
        self.msgs = []
        self.entries = None
        self.labels = None
        self.myButton2 = None
        self.myButton = None
        self.title = None
        self.attack_label = None
        self.params_label = None
        self.attack = None
        self.master = container
        self.myLabel = customtkinter.CTkLabel(self, text='Please specify attack or scenario')
        self.myLabel.pack(ipadx=10, ipady=10)

    def destroy_everything(self):
        if self.extra_label:
            self.extra_label.destroy()
        self.extra_label = None
        if self.myLabel:
            self.myLabel.destroy()
        self.myLabel = None
        if self.myButton:
            self.myButton.destroy()
        self.myButton = None
        if self.myButton2:
            self.myButton2.destroy()
        self.myButton2 = None
        if self.title:
            self.title.destroy()
        self.title = None
        if self.params_label:
            self.params_label.destroy()
        self.params_label = None
        if self.attack_label:
            self.attack_label.destroy()
        self.attack_label = None
        if len(self.msgs) > 0:
            for i in self.msgs:
                i.destroy()
        self.msgs = []
        if self.label:
            self.label.destroy()
        self.label = None
        if self.labels:
            for i in self.labels:
                i.destroy()
        self.labels = None
        if self.entries:
            for i in self.entries:
                i.destroy()
        self.entries = None

    def set_parameters(self, attack):
        self.destroy_everything()
        self.title = customtkinter.CTkLabel(self, text=attack.name())
        self.title.pack()
        if isinstance(attack, scenarios.Scenario):
            self.label = customtkinter.CTkLabel(self, text='The attack will run with randomly generated parameters')
            self.label.pack(padx=5, pady=5)
            self.myButton = customtkinter.CTkButton(self, text="Start Attack", command=self.run_attack)
            self.myButton.pack()
            self.attack = attack
        else:
            self.attack = attack
            needed_params = attack.needed_parameters()

            self.labels = [customtkinter.CTkLabel(self, text=x + ": " + needed_params[x]) for x in needed_params]
            string_vars = [StringVar() for _ in range(len(needed_params))]

            if attack.name() == 'Brute Force Factorization':
                self.entries = [customtkinter.CTkEntry(self, textvariable=string_vars[i]) for i in
                                range(len(needed_params)-1)]
                options = list(attack.get_factorization_options())
                string_vars[-1].set(options[0])
                self.entries.append(OptionMenu(self, string_vars[-1], *options))
            else:
                self.entries = [customtkinter.CTkEntry(self, textvariable=string_vars[i]) for i in
                                range(len(needed_params))]

            for i in range(len(needed_params)):
                if (attack.name() == 'Timing Attack' and i == 2) or (
                        (attack.name() == 'Blinding Attack' or attack.name() == 'Random Fault Attack') and i == 3):
                    self.extra_label = customtkinter.CTkLabel(self, text="⚠ The attacker does not have access to the "
                                                                         "following parameters. They are used as input "
                                                                         "to an oracle that generates the signatures "
                                                                         "required for this attack.", wraplength=500,
                                                              text_color='#3C96D2', text_font='Helvetica 14 bold')
                    self.extra_label.pack(pady=10)
                if (attack.name() == 'Timing Attack' and i >= 2) or (
                        (attack.name() == 'Blinding Attack' or attack.name() == 'Random Fault Attack') and i >= 3):
                    self.labels[i].text_color = '#3C96D2'
                self.labels[i].pack()
                self.entries[i].pack()
            read_input = partial(self.read_input, string_vars, needed_params)
            self.myButton = customtkinter.CTkButton(self, text="Start Attack", command=read_input)
            self.myButton.pack(padx=20, pady=20)
            generate_params = partial(self.generate_params)
            self.myButton2 = customtkinter.CTkButton(self, text="Generate Random Parameters", command=generate_params)
            self.myButton2.pack(padx=20, pady=20)

    def generate_params(self):
        if self.params_label:
            self.params_label.destroy()
        scenario = dict_attacks_scenarios.get(self.attack.__class__)
        sel = scenario()
        _, s, _ = sel.generate_params()
        self.params_label = Text(self, bg='#373737', spacing2=2, highlightbackground='#373737', highlightthickness=0,
                                 height=12, width=60, borderwidth=0)
        self.params_label.insert(1.0, s)
        self.params_label.pack()
        self.params_label.configure(state="disabled")

    def read_input(self, string_vars, needed_params):
        # TODO: replace with string var to update the text
        if self.attack_label:
            self.attack_label.destroy()
        params_to_pass = {}
        i = 0
        for key in needed_params:
            params_to_pass[key] = string_vars[i].get()
            i += 1
        print(params_to_pass)
        self.attack.set_parameters_from_user_input(params_to_pass)
        output, message, _ = self.attack.decrypt()
        self.attack_label = customtkinter.CTkLabel(self, text=message + str(output), wraplength=500)
        self.attack_label.pack(pady=4, padx=10)

    def add_text(self, w):
        s = customtkinter.CTkLabel(self, text=w, wraplength=500)
        self.msgs.append(s)
        s.pack(pady=4, padx=10)

    def run_attack(self):
        if self.attack_label:
            self.attack_label.destroy()
        if self.attack:
            scenario = self.attack.__class__()
            result = scenario.perform()
            for i, word in enumerate(result):
                self.after(1000 * i, lambda w=word: self.add_text(w))
        else:
            self.attack_label = customtkinter.CTkLabel(self, text='Please specify attack or scenario')
            self.attack_label.pack(ipadx=10, ipady=10)
        # Open popup
        pass
