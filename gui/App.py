import customtkinter

from gui.AttacksList import AttacksList
from gui.ParameterFrame import ParameterFrame
from gui.ScenariosList import ScenariosList


class MainFrame(customtkinter.CTkFrame):
    def __init__(self, container):
        super().__init__(container)
        # Setup grid
        self.grid()
        # Add AttackList, ScenariosList and settings in grid
        self.scenarios_list = ScenariosList(self)
        self.scenarios_list.grid(column=0, row=0, padx=10, pady=10)
        self.attacks_list = AttacksList(self)
        self.attacks_list.grid(column=0, row=1, padx=10, pady=10)
        self.parameter_frame = ParameterFrame(self)
        self.parameter_frame.grid(column=1, row=0, rowspan=2, padx=10, pady=10)


        self.pack()


class App(customtkinter.CTk):
    def __init__(self):
        super().__init__()
        super().set_appearance_mode("dark")
        # super().set_default_color_theme("blue")

        self.title('Hacking Lab - Breaking RSA')
        # TODO: add fun icon with self.iconbitmap
        # Add main frame
        MainFrame(self)
