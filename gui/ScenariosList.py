import customtkinter
import scenarios


class ScenariosList(customtkinter.CTkFrame):

    def __init__(self, container):
        super().__init__(container)
        self.outer = container
        self.title = customtkinter.CTkLabel(self, text="Execute a scenario using random parameters  ")
        self.title.pack()

        sc = {cls.__name__: cls for cls in scenarios.Scenario.__subclasses__()}

        for scenario in sc.keys():
            self.myButton = customtkinter.CTkButton(self, text=scenario, command=self.clicker(sc.get(scenario)()))
            self.myButton.pack(padx=5, pady=5)

    def clicker(self, sc):
        def callback():
            self.outer.parameter_frame.set_parameters(sc)
        return callback
