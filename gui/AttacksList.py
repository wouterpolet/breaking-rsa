from tkinter import ttk
import tkinter
import customtkinter

import attack

class AttacksList(customtkinter.CTkFrame):
    def __init__(self, container):
        super().__init__(container)

        self.outer = container
        self.title = customtkinter.CTkLabel(self, text="Execute an attack using your own parameters")
        self.title.pack()

        attacks = {cls.__name__: cls for cls in attack.Attack.__subclasses__()}
        for at in attacks.keys():
            self.myButton = customtkinter.CTkButton(self, text=at, command=self.clicker(attacks.get(at)()))
            self.myButton.pack(padx=5, pady=5)

    def clicker(self, att):
        def callback():
            self.outer.parameter_frame.set_parameters(att)
        return callback
